#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libsocket/libinetsocket.h>

#include "io_util.h"
#include "http_util.h"

/*
 * Returns true if the provided string is a 5-digit zipcode, false otherwise
 */
int isZipcode(char* zipcode) {
    int i = 0;
    while(zipcode[i]) {
        // verify each character is an ascii digit
        char digit = zipcode[i] - 48;
        if(digit < 0 || digit > 9)
            return 0;
        i++;
    }
    // only true if we verified exactly 5 digits
    return i == 5;
}

/*
 * Returns a pointer to where the value of the provided node begins in the json data
 */
char* fake_json_get_node(char* node, char* json) {
    
    // wrap the node in quotes and a semicolon
    char* json_key = malloc((strlen(node) + 4) * sizeof(char));
    sprintf(json_key, "\"%s\":", node);
    
    // pointer to the first character in our json value of choice
    char* value = strstr(json, json_key)+strlen(json_key);
    
    // increment if we need to get inside quotes
    while(value[0] == '"')
        value++;
    
    return value;
}

/*
 * Hunts for the end of the provided json value and terminates it with null
 */
void terminate_json_values(char* value) {
    
    for(int i = 0; value[i]; i++) {
        if(value[i] == '"' || (value[i] == ',' && value[i + 1] == '\n')) {
            value[i] = '\0';
            return;
        }
    }
}

/*
 * Searches and retrieves the first (times) values of the associated node
 */
char** get_json_values(char* node, int times, char* json) {
    
    char** values = malloc(times * sizeof(char*));
    values[times] = fake_json_get_node(node, json);
    
    while(times--) {
        values[times] = fake_json_get_node(node, values[times+1]);
    }
    
    return values;
}

int main(int argc, char *argv[]) {
    
    // complain if we weren't provided the correct amount of arguments
    if (argc < 2) {
        printf("Usage: %s zipcode\n", argv[0]);
        exit(1);
    }
    
    char* zipcode = argv[1];
    
    // complain if the provided argument does NOT constitute a zipcode
    if (!isZipcode(zipcode)) {
        printf("Error: '%s' is NOT a valid 5-digit zipcode\n", zipcode);
        exit(2);
    }
    
    char* url = malloc(80 * sizeof(char));
    sprintf(url, "api.wunderground.com/api/1655f919bbcd29ed/conditions/forecast10day/q/%s.json", zipcode);
    
    char* content_data;
    get_http_content_data(url, &content_data);
    
    char* nodes[] = {
        "observation_time",
        "full",
        "temp_f",
        "relative_humidity",
        "wind_dir",
        "wind_mph"
    };
    int node_count = sizeof(nodes) / sizeof(nodes[0]);
    
    for(int i = 0; i < node_count; i++)
        nodes[i] = fake_json_get_node(nodes[i], content_data);
        
    char* forecasts = strstr(content_data, "simpleforecast");
    
    char** days = get_json_values("weekday", 4, forecasts);
    char** temperatures = get_json_values("fahrenheit", 8, forecasts);
        
    // terminate all our values with null so we can print them
    for(int i = 0; i < node_count; i++)
        terminate_json_values(nodes[i]);
    for(int i = 0; i < 4; i++)
        terminate_json_values(days[i]);
    for(int i = 0; i < 8; i++)
        terminate_json_values(temperatures[i]);
        
    printf("Current Conditions\n");
    printf("Observation time: %s\n", nodes[0]);
    printf("Location: %s\n", nodes[1]);
    printf("Temperature: %s F\n", nodes[2]);
    printf("Humidity: %s\n", nodes[3]);
    printf("Wind: %s %s mph\n", nodes[4], nodes[5]);
    
    printf("\nForecast\n");
    for(int i = 0; i < 4; i++) {
        printf("%s: %s %s\n", days[i], temperatures[i*2], temperatures[i*2 + 1]);
    }
    
}