# -*- indent-tabs-mode:t; -*-

CLANG=clang

weather: weather.o http_util.o io_util.o
	$(CLANG) weather.o http_util.o io_util.o -o weather -l socket

weather.o: weather.c http_util.h io_util.h
	$(CLANG) -c weather.c -Wall

io_util.o: io_util.c io_util.h
	$(CLANG) -c io_util.c -Wall

http_util.o: http_util.c http_util.h io_util.h
	$(CLANG) -c http_util.c -Wall

clean:
	rm -f *.o weather
	
debug: CLANG += -g
debug: weather