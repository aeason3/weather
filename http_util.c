/*
 * AUTHOR: Aaron James Eason
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libsocket/libinetsocket.h>

#include "io_util.h"

/*
 * Parses our http response and returns the content length,
 * setting the file reading position to just before the content stream
 */
int get_http_content_length(FILE* socket) {
    
    int content_length = 0;
    
    // check each line at a time until we hit content
    char line[1000];
    while(fgets(line, 1000, socket) != NULL && line[0] != '\n') {
        // if we find a line that starts with Content-Length
        if(strncmp(line, "Content-Length", 14) == 0) {
            // parse the content length to an integer
            content_length = atoi(strchr(line, ' '));
        }
    }
    
    return content_length;
}

/*
 * Fetches content data from the provided url,
 * reads the data into memory, and returns the length of the data
 *
 * Function does NOT expect a protocol in the url
 */
int get_http_content_data(char* url, char** content_data) {
    
    // points to where the request path begins
    char* request_path = strchr(url, '/');
    
    // complain if the path section could not be found
    if(request_path == NULL) {
        
        printf("Provided url to connect to is malformed: %s\n", url);
        return -1;
    }
    
    // parse the domain into a null-terminated buffer
    int domain_length = request_path - url;
    char* domain = malloc(domain_length + 1);
    strncpy(domain, url, domain_length);
    domain[domain_length+1] = '\0';
    
    // establish a connection to the passed domain
    int socket_descriptor = create_inet_stream_socket(domain, "80", LIBSOCKET_IPv4, 0);
    if(socket_descriptor == -1) {
        printf("Unable to connect to %s\n", domain);
        return -2;
    }
    
    // wrap the connection with a FILE struct to perform access operations
    FILE* socket = fdopen(socket_descriptor, "rb+");
    if(socket == NULL) {
        printf("Couldn't wrap socket descriptor with FILE struct\n");
        return -3;
    }
    
    // send our GET request (and some necessary headers)
    fprintf(socket, "GET %s HTTP/1.0\n", request_path);
    fprintf(socket, "Host: %s\n", domain);
    fprintf(socket, "\n");
    
    // parse for the content length to allocate
    // (and also moves the socket position to the beginning of the content)
    int content_length = get_http_content_length(socket);
    
    // reads the content data into our buffer
    get_file_data_from_file_struct(socket, content_length, content_data);
    
    // clear up all socket connections and dynamically allocated memory
    fclose(socket);
    destroy_inet_socket(socket_descriptor);
    free(domain);
    
    return content_length;
}

