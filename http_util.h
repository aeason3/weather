/*
 * AUTHOR: Aaron James Eason
 */

/*
 * Parses our http response and returns the content length,
 * setting the file reading position to just before the content stream
 */
int get_http_content_length(FILE* socket);

/*
 * Fetches content data from the provided url,
 * reads the data into memory, and returns the length of the data
 *
 * Function does NOT expect a protocol in the url
 */
int get_http_content_data(char* url, char** content_data);