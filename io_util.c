/*
 * AUTHOR: Aaron James Eason
 */
#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>
#include <sys/types.h>

/*
 * Fetches the file length in bytes given the file name
 */
int file_length(char* filename) {
    
    struct stat info;
    int success = stat(filename, &info);
    if(success == -1)
        return -1;
    return info.st_size;
}

/*
 * Reads the file from the provided FILE struct into memory
 */
int get_file_data_from_file_struct(FILE* file, int file_size, char** file_data) {
    
    // check for issues reading the file
    if(!file)
        return -1;
    else if(file_size == -1)
        return -2;
    
    // allocate memory for the input file and read the file into it
    *file_data = malloc(file_size);
    fread(*file_data, sizeof(char), file_size, file);
    
    return 0;
}

/*
 * Reads the file passed into memory and returns the length of the file
 */
int get_file_data(char* file_name, char** file_data) {
    
    // input file information
    int file_size = file_length(file_name);
    FILE *file = fopen(file_name, "r");
    
    // read the file into memory
    int success = get_file_data_from_file_struct(file, file_size, file_data);
    
    // check for issues reading the file
    if(!success) {
        if(success == -1)
            printf("Failed to read %s\n", file_name);
        else if(success == -2)
            printf("Failed to get the size of %s\n", file_name);
        
        return success;
    }
    
    // we probably don't need to, but let's free up this file pointer
    fclose(file);
    
    return file_size;
}

/*
 * Loops through the file data provided, counting file lines
 */
int get_file_line_count(char* file_data, int file_length) {
    
    // keeps track of the number of new lines
    int file_line_count = 0;
    
    // loop through the characters in the file
    for(int i = 0; i < file_length; i++) {
        
        // if we see a new line character
        if(file_data[i] == '\n')
            file_line_count++;
    }
    
    return file_line_count;
}

/*
 * Parses an array of strings for each line in the provided file,
 * replacing newline characters in the file_data with null characters as we go
 *
 * Returns the number of lines in the file
 */
int get_file_lines(char* file_data, int file_length, char*** file_lines) {
    
    // count the number of lines in the file
    int file_line_count = get_file_line_count(file_data, file_length);
    
    // allocate the memory for an array of pointers to each line
    (*file_lines) = malloc(file_line_count * sizeof(char*));
    
    // we already know where the first line starts :)
    (*file_lines)[0] = &file_data[0];
    
    // tracks which line we're currently looking for
    int line_index = 1;
    
    // loop through the characters in the file
    for(int i = 0; i < file_length; i++) {
        
        // if we see a new line character
        if(file_data[i] == '\n') {
            
            // replace with the null character
            file_data[i] = '\0';
            
            // track the upcoming line, but only if we haven't run out of lines to store
            if(line_index < file_line_count)
                (*file_lines)[line_index++] = &file_data[i+1];
        }
    }
    
    return file_line_count;
}

/*
 * Writes data to the file if and only if the file is defined
 */
void print_line_to_file_or_stdout(FILE* output_file, char* data) {
    
    if(output_file) {
        fprintf(output_file, "%s\n", data);
    } else {
        printf("%s\n", data);
    }
}